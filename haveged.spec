Summary:        A Linux entropy source using the HAVEGE algorithm
Name:           haveged
Version:        1.9.19
Release:        1
License:        GPL-3.0-or-later
URL:            https://github.com/jirka-h/haveged
Source0:        https://github.com/jirka-h/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

Patch0:         fix-the-core-file-problem.patch
# patch from openSUSE
Patch1:         haveged-1.9.19-harden_haveged.service.patch
%{?systemd_requires}
BuildRequires:  gcc
BuildRequires:  systemd

%description
A Linux entropy source using the HAVEGE algorithm

Haveged is a user space entropy daemon which is not dependent upon the
standard mechanisms for harvesting randomness for the system entropy
pool. This is important in systems with high entropy needs or limited
user interaction (e.g. headless servers).
 
Haveged uses HAVEGE (HArdware Volatile Entropy Gathering and Expansion)
to maintain a 1M pool of random bytes used to fill /dev/random
whenever the supply of random bits in /dev/random falls below the low
water mark of the device. The principle inputs to haveged are the
sizes of the processor instruction and data caches used to setup the
HAVEGE collector. The haveged default is a 4kb data cache and a 16kb
instruction cache. On machines with a cpuid instruction, haveged will
attempt to select appropriate values from internal tables.

%package devel
Summary:   Headers and shared development libraries for HAVEGE algorithm
Requires:  %{name} = %{version}-%{release}

%description devel
Headers and shared object symbolic links for the HAVEGE algorithm

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-enttest --enable-nistest --disable-static
%make_build -j1

%check
%make_build check -j1

%install
%make_install
%delete_la

#Install systemd service file
sed -e 's:@SBIN_DIR@:%{_sbindir}:g' -i contrib/Fedora/*service
sed -i '/^ConditionKernelVersion/d' contrib/Fedora/*service

# NOTICE: DO NOT push switch-root service due to upstream issue#77
install -Dpm 0644 contrib/Fedora/haveged.service %{buildroot}%{_unitdir}/%{name}.service
install -Dpm 0755 contrib/Fedora/haveged-dracut.module %{buildroot}/%{_prefix}/lib/dracut/modules.d/98%{name}/module-setup.sh
install -Dpm 0644 contrib/Fedora/90-haveged.rules %{buildroot}%{_udevrulesdir}/90-%{name}.rules

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license COPYING
%doc README ChangeLog AUTHORS
%{_sbindir}/haveged
%{_unitdir}/*.service
%{_libdir}/*so.*
%{_udevrulesdir}/*-%{name}.rules
%dir %{_prefix}/lib/dracut/modules.d/98%{name}
%{_prefix}/lib/dracut/modules.d/98%{name}/*

%files devel
%{_includedir}/%{name}
%{_libdir}/*.so

%files help
%doc contrib/build/havege_sample.c
%{_mandir}/man?/*

%changelog
* Fri Oct 04 2024 Funda Wang <fundawang@yeah.net> - 1.9.19-1
- update to 1.9.19
- use upstream systemd unit with hardened patch from openSUSE

* Wed Sep 6 2023 wangqingsan <wangqingsan@huawei.com> - 1.9.18-2
- enable haveged.service

* Mon Jul 4 2022 panxiaohe <panxh.life@foxmail.com> - 1.9.18-1
- update to 1.9.18

* Fri Dec 24 2021 yixiangzhike <yixiangzhike007@163.com> - 1.9.15-1
- update to 1.9.15

* Mon Jul 19 2021 yixiangzhike <zhangxingliang3@huawei.com> - 1.9.14-3
- delete unnecessary gdb from BuildRequires

* Tue May 25 2021 yixiangzhike <zhangxingliang3@huawei.com> - 1.9.14-2
- add gcc to BuildRequires

* Tue Jan 26 2021 fuanan <fuanan3@huawei.com> - 1.9.14-1
- Update to version 1.9.14

* Fri Sep 11 2020 zhangxingliang <zhangxingliang3@huawei.com> - 1.9.13-3
- solve startup failure

* Thu Aug 13 2020 Anakin Zhang <benjamin93@163.com> - 1.9.13-2
- remove libhavege.so.1*

* Thu Jul 23 2020 wangchen <wangchen137@huawei.com> - 1.9.13-1
- update to 1.9.13

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.9.8-2
- update service

* Mon Oct 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.8-1
- update to 1.9.8-1

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.1-3
- fix the core file problem

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.1-2
- adjust requires

* Wed Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.1-1
- rewrite spec file based upstream spec
